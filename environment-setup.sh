# LAMP
sudo yum update -yum
sudo yum groupinstall -y "Web Server" "MySQL Client" "PHP Support"
sudo yum install -y php-mysql git
sudo service httpd start
sudo chkconfig httpd on
chkconfig --list httpd
# Environment variables
mkdir dev
mkdir dev/git
$(cd dev/git && git clone https://thomas_scott@bitbucket.org/thomas_scott/linux-bashrc.git )
$(cd dev/git && git clone https://thomas_scott@bitbucket.org/thomas_scott/bin.git )
ln -s ~/dev/git/bin/ bin
sudo rm -rf .bashrc
ln -s ~/dev/git/linux-bashrc/bashrc .bashrc
bash


