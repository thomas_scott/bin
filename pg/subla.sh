#!/bin/bash

for i in "$(find . -maxdepth 1 -type f)"; do
	subl "$i";
done