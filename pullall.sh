#!/bin/bash

list=$(find ~/dev/git/ -type d -maxdepth 1)
for i in $list
do
	echo $!":" &&cd $i && git pull
done