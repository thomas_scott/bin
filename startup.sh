sudo yum update -y
sudo yum group install "Web Server" "PHP Support" -y
sudo yum install git tree htop php-mysql -y

rm -rf .bashrc
mkdir dev
mkdir dev/git
cd dev/git
git clone https://thomas_scott@bitbucket.org/thomas_scott/linux-bashrc.git
cd
ln -s ~/dev/git/linux-bashrc/bashrc .bashrc
bash
sudo chkconfig httpd on